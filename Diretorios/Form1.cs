﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Diretorios
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DirectoryInfo diretorio = new DirectoryInfo(@"C:/diretorio");

            try
            {
                if (diretorio.Exists)
                {
                    MessageBox.Show("Existe");
                }
                else
                {
                    MessageBox.Show("Não Existe - Será Criado");
                    diretorio.Create();
                }
            }
            catch (IOException erro)
            {
                MessageBox.Show("Erro ao criar o diretório: " + erro);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DirectoryInfo diretorio = new DirectoryInfo(@"C:/diretorio");

            try
            {
                if (diretorio.Exists)
                {
                    MessageBox.Show("Existe vou excluir");
                    diretorio.Delete();
                    MessageBox.Show("Excluido");
                }
            }
            catch (IOException erro)
            {
                MessageBox.Show("Erro ao criar o diretório: " + erro);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DirectoryInfo diretorio = new DirectoryInfo(@"C:/diretorio");

            try
            {
                if (diretorio.Exists)
                {
                    MessageBox.Show("Existe vou criar o subdiretorio");
                    diretorio.CreateSubdirectory(@"subdiretorio");
                    diretorio.CreateSubdirectory(@"subdiretorio\anus\bundinha");
                    MessageBox.Show("Criado");
                }
            }
            catch (IOException erro)
            {
                MessageBox.Show("Erro ao criar o diretório: " + erro);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DirectoryInfo diretorio = new DirectoryInfo(@"C:/diretorio");

            try
            {
                if (diretorio.Exists)
                {
                    MessageBox.Show("Diretório: " + diretorio.FullName);
                }
            }
            catch (IOException erro)
            {
                MessageBox.Show("Erro ao criar o diretório: " + erro);
            }
        }
    }
}
